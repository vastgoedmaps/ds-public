'use strict';

const request = require('request');
const qs = require('qs');

class Client {

  /**
   * Constructs JS client for REST API
   * 
   * @return
   */

  constructor(opts) {
    this.url = opts.url || 'http://localhost:8000';
    this.defaultParameters = {
      key: opts.key,
      secret: opts.secret
    };
  }

  /**
   * Format and make request to API
   * 
   * @param  {Object}   opts     Request parameters
   * @param  {Function} callback 
   * @return
   */

  makeRequest(opts, callback) {
    var params = {
      method: opts.method || 'GET',
      url: this.url + opts.path,
      json: true
    };

    if (params.method === 'POST') {
      params.formData = opts.opts;
    }

    if (params.method === 'GET' || params.method === 'PUT' || params.method === 'DELETE') {
      var query = opts.opts ? '?' + qs.stringify(opts.opts) : '';
      params.url += query;
    }

    params.url += (opts.opts && params.method !== 'POST' ? '&' : '?') + qs.stringify(this.defaultParameters);

    request(params, (err, res, body) => {
      if (err) return callback(err, null);
      if (!body) {
        return callback('Error got empty body as respone', null);
      }
      if (body.error) return callback(body.error, null);
      return callback(null, body.result);
    });
  }

  /**
   * Ping API
   * 
   * @param  {Function} callback
   * @return
   */

  ping(callback) {
    var params = {
      path: '/ping'
    };

    this.makeRequest(params, callback);
  }

  /**
   * Get all files
   * 
   * @param  {Object}   opts     Request parameters
   * @param  {Function} callback
   * @return
   */

  getFiles(opts, callback) {
    var params = {
      path: '/files',
      opts: opts || {}
    };

    this.makeRequest(params, callback);
  }

  /**
   * Search for files
   * 
   * @param  {Object}   opts     Request parameters
   * @param  {Function} callback
   * @return
   */

  search(opts, callback) {
    var params = {
      path: '/search',
      opts: opts || {}
    };

    this.makeRequest(params, callback);
  }

  /**
   * Get singular files
   * 
   * @param  {Object}   opts     Request parameters
   * @param  {Function} callback
   * @return
   */

  getFile(opts, callback) {
    var params = {
      path: '/file',
      opts: opts || {}
    };

    this.makeRequest(params, callback);
  }

  /**
   * Upload file
   * 
   * @param  {Object}   opts     Request parameters
   * @param  {Function} callback
   * @return
   */

  uploadFile(opts, callback) {
    var params = {
      method: 'POST',
      path: '/file',
      opts: opts || {}
    };

    this.makeRequest(params, callback);
  }

  /**
   * Update file
   * 
   * @param  {Object}   opts     Request parameters
   * @param  {Function} callback
   * @return
   */

  updateFile(opts, callback) {
    var params = {
      method: 'PUT',
      path: '/file',
      opts: opts || {}
    };

    this.makeRequest(params, callback);
  }

  /**
   * Delete file
   * 
   * @param  {Object}   opts     Request parameters
   * @param  {Function} callback
   * @return
   */

  softDeleteFile(opts, callback) {
    var params = {
      method: 'DELETE',
      path: '/file',
      opts: opts || {}
    };

    this.makeRequest(params, callback);
  }

  /**
   * Resolve file to ObjectStore URI
   * 
   * @param  {Object}   opts     Request parameters
   * @param  {Function} callback
   * @return
   */

  resolve(opts, callback) {
    var params = {
      path: '/resolve',
      opts: opts || {}
    };

    this.makeRequest(params, callback);
  }

  /**
   * Get stats about store
   * 
   * @param  {Object}   opts     Request parameters
   * @param  {Function} callback
   * @return
   */

  stats(opts, callback) {
    var params = {
      path: '/stats',
      opts: opts || {}
    };

    this.makeRequest(params, callback);
  }

  /**
   * Should get all trashed files for user
   * 
   * @param  {Object}   opts     Request parameters
   * @param  {Function} callback
   * @return
   */

  trash(opts, callback) {
    var params = {
      method: 'GET',
      path: '/files/trash',
      opts: opts || {}
    };

    this.makeRequest(params, callback);
  }

  /**
   * Restore a file from trash
   * 
   * @param  {Object}   opts     Request parameters
   * @param  {Function} callback
   * @return
   */

  restoreFile(opts, callback) {
    var params = {
      method: 'PUT',
      path: '/files/trash',
      opts: opts || {}
    };

    this.makeRequest(params, callback);
  }

  /**
   * Hard delete a specific file: Remove file from Object Store, Deindex and set file to deled
   * 
   * @param  {Object}   opts     Request parameters
   * @param  {Function} callback
   * @return
   */

  hardDeleteFile(opts, callback) {
    var params = {
      method: 'DELETE',
      path: '/files/trash',
      opts: opts || {}
    };

    this.makeRequest(params, callback);
  }

  /**
   * Get user
   * 
   * @param  {Object}   opts     Request parameters
   * @param  {Function} callback
   * @return
   */

  getUser(opts, callback) {
    var params = {
      method: 'GET',
      path: '/users',
      opts: opts || {}
    };

    this.makeRequest(params, callback);
  }

  /**
   * Create user
   * 
   * @param  {Object}   opts     Request parameters
   * @param  {Function} callback
   * @return
   */

  createUser(opts, callback) {
    var params = {
      method: 'POST',
      path: '/users',
      opts: opts || {}
    };

    this.makeRequest(params, callback);
  }

  /**
   * Update user
   * 
   * @param  {Object}   opts     Request parameters
   * @param  {Function} callback
   * @return
   */

  updateUser(opts, callback) {
    var params = {
      method: 'PUT',
      path: '/users',
      opts: opts || {}
    };

    this.makeRequest(params, callback);
  }

  /**
   * Delete user
   * 
   * @param  {Object}   opts     Request parameters
   * @param  {Function} callback
   * @return
   */

  deleteUser(opts, callback) {
    var params = {
      method: 'DELETE',
      path: '/users',
      opts: opts || {}
    };

    this.makeRequest(params, callback);
  }

}

module.exports = Client;
